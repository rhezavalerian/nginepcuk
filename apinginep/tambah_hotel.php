<?php
 
require_once 'include/functions_hotel.php';
$db = new hotel();
$response = array("error" => FALSE);
if (isset($_POST['nama']) && isset($_POST['provinsi']) && isset($_POST['kota']) && isset($_POST['alamat']) && isset($_POST['lat']) && isset($_POST['lng']) && isset($_POST['telp']) && isset($_POST['email'])) {
 
    $nama = $_POST['nama'];
    $provinsi = $_POST['provinsi'];
    $kota = $_POST['kota'];
    $alamat = $_POST['alamat'];
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $telp = $_POST['telp'];
    $email = $_POST['email'];

    if ($nama == "" || $provinsi == "" || $kota == "" || $alamat == "" || $lat == "" || $lng == "" || $telp == "" || $email == "" ) {
        $response["error"] = TRUE;
        $response["code"] = "-300";
        $response["msg"] = "Slow down cowboy! .Pastikan anda telah mengisi semua form diatas! ";
        echo json_encode($response);
    }else{
        if ($db->cekHotel($nama, $kota)) {
            $response["error"] = TRUE;
            $response["code"] = "-301";
            $response["msg"] = "Ups sepertinya hotel dengan nama " . $nama . " sudah digunakan di kota " . $kota . " ." ;
            echo json_encode($response);
        } else {
            if($db->cekLoc($lat, $lng)) {
                $response["error"] = TRUE;
                $response["code"] = "-302";
                $response["msg"] = "Ups sepertinya hotel dengan lokasi yang anda masukan sudah didaftarkan ";
                echo json_encode($response);
            } else{
            $hotel = $db->simpanHotel($nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email);
            if ($hotel) {
                $response["error"] = FALSE;
                $response["code"] = "0";
                $response["msg"] = "Registrasi Hotel Berhasil!";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["code"] = "-303";
                $response["msg"] = "Ups terjadi kesalahan saat melakukan registrasi. Cek jaringan internet anda!";
                echo json_encode($response);
            }
            }
        }
    }
} else {
    $response["error"] = TRUE;
    $response["errorcode"] = "-305";
    $response["error_msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>