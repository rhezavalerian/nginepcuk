<?php

require_once 'include/functions_logreg.php';
$db = new logreg();
$response = array("error" => FALSE);
 
if (isset($_POST['username']) && isset($_POST['password'])) {
 
    $username = $_POST['username'];
    $password = $_POST['password'];
 
    $user = $db->cekUserPassword($username, $password);
 
    if ($user != false) {
        $response["error"] = FALSE;
        $response["code"] = "0";
        $response["msg"] = "Login Berhasil!";
        $response["user"]["email"] = $user["email"];
        $response["user"]["username"] = $username;
        $response["user"]["nama"] = $user["nama"];
        $response["user"]["points"] = $user["points"];
        echo json_encode($response);
    } elseif ($password == "" && $username == "") {
        $response["error"] = TRUE;
        $response["code"] = "-103";
        $response["msg"] = "Login gagal! . Username dan Password tidak boleh kosong!";
        echo json_encode($response);
    } elseif ($username == "") {
        $response["error"] = TRUE;
        $response["code"] = "-101";
        $response["msg"] = "Login gagal! . Username tidak boleh kosong!";
        echo json_encode($response);
    } elseif ($password == "") {
        $response["error"] = TRUE;
        $response["code"] = "-102";
        $response["msg"] = "Login gagal! . Password tidak boleh kosong!";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["code"] = "-104";
        $response["msg"] = "Login gagal! . password atau username salah";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["code"] = "-105";
    $response["msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>