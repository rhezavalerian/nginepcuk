-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.25-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5127
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for jalanjalandb
CREATE DATABASE IF NOT EXISTS `jalanjalandb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jalanjalandb`;

-- Dumping structure for table jalanjalandb.booked
CREATE TABLE IF NOT EXISTS `booked` (
  `id_booked` varchar(50) NOT NULL,
  `id_pembayaran` varchar(50) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `tgl_booked` date NOT NULL,
  `tgl_checkin` date NOT NULL,
  `tgl_checkout` date NOT NULL,
  `lama` int(11) NOT NULL,
  `jmlh_kamar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.booked: ~0 rows (approximately)
/*!40000 ALTER TABLE `booked` DISABLE KEYS */;
/*!40000 ALTER TABLE `booked` ENABLE KEYS */;

-- Dumping structure for table jalanjalandb.hotel_data
CREATE TABLE IF NOT EXISTS `hotel_data` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `lat` varchar(50) DEFAULT NULL,
  `lng` varchar(50) DEFAULT NULL,
  `telp` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.hotel_data: ~8 rows (approximately)
/*!40000 ALTER TABLE `hotel_data` DISABLE KEYS */;
REPLACE INTO `hotel_data` (`id`, `nama`, `provinsi`, `kota`, `alamat`, `lat`, `lng`, `telp`, `email`, `flag`) VALUES
	(0000000001, 'Tes', 'Jawa Timur', 'Surabaya', 'jalan suka jalan', '-7.279379022306316', 'Surabaya', '12357484', 'yogy.rd24@gmail.com', 0),
	(0000000002, 'Hotel Bumi', 'Jawa Timur', 'Surabaya', 'Jl. Paramuka No 31', '-7.27801679779765', '112.72384643554688', '1234567879', 'ray_susanto3@yahoo.com', 0),
	(0000000003, 'Tessdfsdf', 'Jawa Timur', 'Surabaya', 'Jl. Gading Timur Jakarata', '-7.293001039730459', '112.70462036132812', '12321343243', 'ydwimniez@yahoo.com', 0),
	(0000000004, 'Hotel Melati', 'Jawa Timur', 'Surabaya', 'jalan suka jalan', '-7.079088026071719', '112.24868774414062', '01774573842324', 'mitramedicare.it@gmail.com', 0),
	(0000000005, 'Hotel Mawar', 'Jawa Timur', 'Surabaya', 'Jl. Paramuka No 31', '-7.130872363729571', '112.22122192382812', '1234567879', 'ydwimniez@yahoo.com', 0),
	(0000000006, 'asdsadasd', 'Jawa Timur', 'Surabaya', 'asdsadasd', '-7.042290356532728', '112.1978759765625', 'asdasdasd', 'asdad', 0),
	(0000000007, 'Hotel Suyra', 'Jawa Timur', 'Surabaya', 'Jl. Paramuka No 31', '-7.312071167657927', '112.71148681640625', '08794575834', 'yogy.rd24@gmail.com', 0),
	(0000000008, 'Yogy Rachmad Tes', 'Jawa Timur', 'Surabaya', 'Jl. Paramuka No 31', '-7.280741242677959', '112.72109985351562', '1234567879', 'yogy.rd24@gmail.com', 0);
/*!40000 ALTER TABLE `hotel_data` ENABLE KEYS */;

-- Dumping structure for table jalanjalandb.kota
CREATE TABLE IF NOT EXISTS `kota` (
  `id_kota` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.kota: ~0 rows (approximately)
/*!40000 ALTER TABLE `kota` DISABLE KEYS */;
/*!40000 ALTER TABLE `kota` ENABLE KEYS */;

-- Dumping structure for table jalanjalandb.pembayaran
CREATE TABLE IF NOT EXISTS `pembayaran` (
  `id_pembayaran` varchar(50) NOT NULL,
  `id_booked` varchar(50) NOT NULL,
  `tgl_cetak` date NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `kode_unik` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.pembayaran: ~0 rows (approximately)
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;

-- Dumping structure for table jalanjalandb.review
CREATE TABLE IF NOT EXISTS `review` (
  `id_review` varchar(15) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  `uraian` varchar(100) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.review: ~0 rows (approximately)
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;

-- Dumping structure for table jalanjalandb.tipe_kamar
CREATE TABLE IF NOT EXISTS `tipe_kamar` (
  `id_tipe` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL,
  `jenis_kamar` varchar(50) NOT NULL,
  `fasilitas` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.tipe_kamar: ~0 rows (approximately)
/*!40000 ALTER TABLE `tipe_kamar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipe_kamar` ENABLE KEYS */;

-- Dumping structure for table jalanjalandb.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(20) NOT NULL,
  `points` int(11) NOT NULL,
  `flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table jalanjalandb.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `username`, `nama`, `alamat`, `email`, `encrypted_password`, `salt`, `points`, `flag`) VALUES
	(5, 'Glitch', 'Rheza Valerian', NULL, 'rvalerian@outlook.com', '+GYBhw89IZNeF1DJG6thfb0HApNjMTA4MmFjZjNk', 'c1082acf3d', 0, 1),
	(6, 'Glitch1', 'test', NULL, 'test@mail.com', 'uFQ2994Kf7Gw6fvgNPARLF8bCOcwOGI2YjUxYzRk', '08b6b51c4d', 0, 0),
	(7, 'yogyrd', 'Yogy Rachmad Dharmawan', NULL, 'me@ogikomkom.com', '+GYBhw89IZNeF1DJG6thfb0HApNjMTA4MmFjZjNk', 'c1082acf3d', 0, 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
