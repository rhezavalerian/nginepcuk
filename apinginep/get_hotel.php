<?php
 
require_once 'include/functions_hotel.php';
$db = new hotel();
$response = array("error" => FALSE);
if (isset($_POST['idhotel'])) {
 
    $provinsi = $_POST['idhotel'];

    if ($provinsi == "") {
        $response["error"] = TRUE;
        $response["code"] = "-410";
        $response["msg"] = "Slow down cowboy! .Pastikan anda ingin mencari hotel! ";
        echo json_encode($response);
    }else{
        $hotel = $db->getHotel($provinsi);
        if ($hotel) {
            $response["error"] = FALSE;
            $response["code"] = "0";
            $response["msg"] = "Pencarian Berhasil!";
            $response["hasil"] = $hotel;
            echo json_encode($response);
        } else {
            $response["error"] = TRUE;
            $response["code"] = "-411";
            $response["msg"] = "Ups sepertinya kami tidak dapat menemukan hotel yang anda cari! ";
            echo json_encode($response);
        }
    }
} else {
    $response["error"] = TRUE;
    $response["errorcode"] = "-409";
    $response["error_msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>