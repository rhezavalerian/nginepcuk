<?php

/* 
 

    Master Hotel
    Change Log :
               
                - (+) get hotel by id
                - (+) filter kota Hotel
                - (+) edit hotel
                - (+) filter provinsi Hotel
                - (+) filter nama Hotel (bug cuma 1 hotel) (fixed)
                - (+) cek lokasi Hotel
                - (+) cek nama Hotel
                - (+) tambah Hotel



 */

 class hotel {
    private $conn;
    function __construct() {
        require_once 'connect_me.php';
        $db = new connect_me();
        $this->conn = $db->connect();
    }
 
    function __destruct() {
         
    }

    public function getHotel($id) {
        $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE id = ?");
        $stmt->bind_param("s", $id);
        $statistic = [];
        if ($stmt->execute()) {
            $result  = $stmt->get_result();
            while ($data = $result->fetch_assoc())
            {
                $statistic[] = $data;
            }
           return $statistic;
            $stmt->close();
        } else {
            return Null;
        }
    }

    public function filterHotelNama($nama) {
        $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE nama = ?");
        $stmt->bind_param("s", $nama);
        $statistic = [];
        if ($stmt->execute()) {
            $result  = $stmt->get_result();
            while ($data = $result->fetch_assoc())
            {
                $statistic[] = $data;
            }
           return $statistic;
            $stmt->close();
        } else {
            return Null;
        }
    }
    
    public function filterHotelProvinsi($provinsi) {
        $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE provinsi = ?");
        $stmt->bind_param("s", $provinsi);
        $statistic = [];
        if ($stmt->execute()) {
            $result  = $stmt->get_result();
            while ($data = $result->fetch_assoc())
            {
                $statistic[] = $data;
            }
           return $statistic;
            $stmt->close();
        } else {
            return Null;
        }
    }

    public function filterHotelKota($kota) {
        $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE kota = ?");
        $stmt->bind_param("s", $kota);
        $statistic = [];
        if ($stmt->execute()) {
            $result  = $stmt->get_result();
            while ($data = $result->fetch_assoc())
            {
                $statistic[] = $data;
            }
           return $statistic;
            $stmt->close();
        } else {
            return Null;
        }
    }


  /*  public function filterHotel($nama, $provinsi) {
        
    }

    public function filterHotel($nama, $provinsi, $kota) {
        
    }

    public function loadHotel($nama, $provinsi, $kota) {
        
    } */

    public function cekHotel($nama,$kota) {
        $stmt = $this->conn->prepare("SELECT nama from hotel_data WHERE nama = ? AND kota = ?");
        $stmt->bind_param("ss", $nama ,$kota);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function cekLoc($lat, $lng) {
        $stmt = $this->conn->prepare("SELECT lat,lng from hotel_data WHERE lat = ? AND lng = ?");
        $stmt->bind_param("ss", $lat ,$lng);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function simpanHotel($nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email) {
        $id   = '';
        $flag = 0;
        $stmt = $this->conn->prepare("INSERT INTO hotel_data(id, nama, provinsi, kota, alamat, lat, lng, telp, email, flag) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssss", $id, $nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email, $flag);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE nama = ?");
            $stmt->bind_param("s", $nama);
            $stmt->execute();
            $hotel = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $hotel;
        } else {
            return false;
        }
    }

    public function editHotel($id, $nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email) {

        $stmt = $this->conn->prepare("UPDATE hotel_data SET nama = ?, provinsi = ?, kota = ?, alamat = ?, lat = ?, lng = ?, telp = ?, email = ? WHERE id = ?");
        $stmt->bind_param("sssssssss", $nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email, $id);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE nama = ?");
            $stmt->bind_param("s", $nama);
            $stmt->execute();
            $hotel = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $hotel;
        } else {
            return false;
        }
    }

 }
?>