<?php

/* 
 

    Master Hotel
    Change Log :
               
                - (+) filter provinsi Hotel
                - (+) filter nama Hotel (bug cuma 1 hotel)
                - (+) cek lokasi Hotel
                - (+) cek nama Hotel
                - (+) tambah Hotel



 */

 class hotel {
    private $conn;
    function __construct() {
        require_once 'connect_me.php';
        $db = new connect_me();
        $this->conn = $db->connect();
    }
 
    function __destruct() {
         
    }

    public function filterHotelNama($nama) {
        $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE nama = ?");
        $stmt->bind_param("s", $nama);
        if ($stmt->execute()) {
            $hotel = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            if ($hotel > 0 ) {
                return $hotel;
            }else{
                return False;
            }
        } else {
            return Null;
        }
    }
    
    public function filterHotelProvinsi($provinsi) {
        $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE provinsi = ?");
        $stmt->bind_param("s", $provinsi);
        $statistic = [];
        if ($stmt->execute()) {
            $result  = $stmt->get_result();
            while ($data = $result->fetch_assoc())
            {
                $statistic[] = $data;
            }
           return $statistic;
            $stmt->close();
        } else {
            return Null;
        }
    }

  /*  public function filterHotel($nama, $provinsi) {
        
    }

    public function filterHotel($nama, $provinsi, $kota) {
        
    }

    public function loadHotel($nama, $provinsi, $kota) {
        
    } */

    public function cekHotel($nama) {
        $stmt = $this->conn->prepare("SELECT nama from hotel_data WHERE nama = ?");
        $stmt->bind_param("s", $nama);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function cekLoc($lat, $lng) {
        $stmt = $this->conn->prepare("SELECT lat,lng from hotel_data WHERE lat = ? AND lng = ?");
        $stmt->bind_param("ss", $lat ,$lng);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function simpanHotel($nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email) {
        $id   = '';
        $flag = 0;
        $stmt = $this->conn->prepare("INSERT INTO hotel_data(id, nama, provinsi, kota, alamat, lat, lng, telp, email, flag) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssss", $id, $nama, $provinsi, $kota, $alamat, $lat, $lng, $telp, $email, $flag);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM hotel_data WHERE nama = ?");
            $stmt->bind_param("s", $nama);
            $stmt->execute();
            $hotel = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $hotel;
        } else {
            return false;
        }
    }

 }

 /* 
 

    Login dan Register
    Change Log :
                - (+) Variabel Telp
                - (+) Salt Kunci Enkripsi password
                - (+) HashSSHA Enkripsi password
                - (+) Cek Login User
                - (+) Cek Email tersedia
                - (+) Cek Username tersedia



 */

class logreg {
    private $conn;
    function __construct() {
        require_once 'connect_me.php';
        $db = new connect_me();
        $this->conn = $db->connect();
    }
 
    function __destruct() {
         
    }
 
    public function simpanUser($username, $nama, $email, $password) {
        $id   = '';
        $zero = 0;
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; 
        $salt = $hash["salt"]; 

        $stmt = $this->conn->prepare("INSERT INTO user(id, username, nama, email, encrypted_password, salt, no_tlp, points, flag) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssssssss", $id, $username, $nama, $email, $encrypted_password, $salt, $zero, $zero, $zero);

        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }

    public function cekUserPassword($username, $password) {
 
        $stmt = $this->conn->prepare("SELECT * FROM user WHERE username = ?");
        $stmt->bind_param("s", $username);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            $salt = $user['salt'];
            $encrypted_password = $user['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
            if ($encrypted_password == $hash) {
                return $user;
            }
        } else {
            return NULL;
        }
    }
 
   
    public function cekEmail($email) {
        $stmt = $this->conn->prepare("SELECT email from user WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }
 
    public function cekUser($username) {
        $stmt = $this->conn->prepare("SELECT username from user WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
        return $hash;
    }
 
}
 
?>