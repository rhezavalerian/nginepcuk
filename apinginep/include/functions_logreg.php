<?php

/* 
 

    Login dan Register
    Change Log :
                - (+) Ganti Password
                - (+) Edit Username
                - (+) Variabel Telp
                - (+) Salt Kunci Enkripsi password
                - (+) HashSSHA Enkripsi password
                - (+) Cek Login User
                - (+) Cek Email tersedia
                - (+) Cek Username tersedia



 */

class logreg {
    private $conn;
    function __construct() {
        require_once 'connect_me.php';
        $db = new connect_me();
        $this->conn = $db->connect();
    }
 
    function __destruct() {
         
    }
 
    public function simpanUser($username, $nama, $email, $password) {
        $id   = '';
        $zero = 0;
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; 
        $salt = $hash["salt"]; 

        $stmt = $this->conn->prepare("INSERT INTO user(id, username, nama, email, encrypted_password, salt, no_tlp, points, flag) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssssssss", $id, $username, $nama, $email, $encrypted_password, $salt, $zero, $zero, $zero);

        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }



    public function editUsername($usernamebaru , $usernamelama) {
      
        $stmt = $this->conn->prepare("UPDATE user SET username = ? WHERE username = ?");
        $stmt->bind_param("ss", $usernamebaru, $usernamelama);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user WHERE username = ?");
            $stmt->bind_param("s", $usernamebaru);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }

    public function gantiPassword($username,$passwordbaru) {
        $hash = $this->hashSSHA($passwordbaru);
        $encrypted_password = $hash["encrypted"]; 
        $salt = $hash["salt"]; 

        $stmt = $this->conn->prepare("UPDATE user SET encrypted_password = ? , salt = ? WHERE username = ?");
        $stmt->bind_param("sss", $encrypted_password, $salt, $username);

        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user WHERE encrypted_password = ?");
            $stmt->bind_param("s", $encrypted_password);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }


    public function editTelp($numb,$username) {
        
          $stmt = $this->conn->prepare("UPDATE user SET no_tlp = ? WHERE username = ?");
          $stmt->bind_param("ss", $numb, $username);
          $result = $stmt->execute();
          $stmt->close();
          if ($result) {
              $stmt = $this->conn->prepare("SELECT * FROM user WHERE no_tlp = ?");
              $stmt->bind_param("s", $numb);
              $stmt->execute();
              $user = $stmt->get_result()->fetch_assoc();
              $stmt->close();
              return $user;
          } else {
              return false;
          }
      }
      
    public function editEmail($emailbaru , $emaillama) {
        
          $stmt = $this->conn->prepare("UPDATE user SET email = ? WHERE email = ?");
          $stmt->bind_param("ss", $emailbaru, $emaillama);
          $result = $stmt->execute();
          $stmt->close();
          if ($result) {
              $stmt = $this->conn->prepare("SELECT * FROM user WHERE email = ?");
              $stmt->bind_param("s", $emailbaru);
              $stmt->execute();
              $user = $stmt->get_result()->fetch_assoc();
              $stmt->close();
              return $user;
          } else {
              return false;
          }
      }

    public function cekUserPassword($username, $password) {
 
        $stmt = $this->conn->prepare("SELECT * FROM user WHERE username = ?");
        $stmt->bind_param("s", $username);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            $salt = $user['salt'];
            $encrypted_password = $user['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
            if ($encrypted_password == $hash) {
                return $user;
            }
        } else {
            return NULL;
        }
    }
 
   
    public function cekEmail($email) {
        $stmt = $this->conn->prepare("SELECT email from user WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }
 
    public function cekTlp($tlp) {
        $stmt = $this->conn->prepare("SELECT no_tlp from user WHERE no_tlp = ?");
        $stmt->bind_param("s", $tlp);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function cekUser($username) {
        $stmt = $this->conn->prepare("SELECT username from user WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
        return $hash;
    }
 
}
 
?>