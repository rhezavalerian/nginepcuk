<?php
 
require_once 'include/functions_logreg.php';
$db = new logreg();
$response = array("error" => FALSE);
 
if (isset($_POST['notlp']) && isset($_POST['username'])) {
 
    $tlp = $_POST['notlp'];
    $username = $_POST['username'];

    if ($tlp == "" || $username == "") {
        $response["error"] = TRUE;
        $response["code"] = "-215";
        $response["msg"] = "Slow down cowboy! .Pastikan anda telah mengisi semua form diatas! ";
        echo json_encode($response);
    }else{
            if($db->cekTlp($tlp)) {
                $response["error"] = TRUE;
                $response["code"] = "-216";
                $response["msg"] = "Ups sepertinya nomor " . $tlp . " telah digunakan oleh akun lain! ";
                echo json_encode($response);
            } else{
            $user = $db->editTelp($tlp, $username);
            if ($user) {
                $response["error"] = FALSE;
                $response["code"] = "0";
                $response["msg"] = "Nomor telepon berhasil didaftarkan!";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["code"] = "-217";
                $response["msg"] = "Terjadi kesalahan saat melakukan registrasi. Cek jaringan internet anda!";
                echo json_encode($response);
            }
            }
    }
} else {
    $response["error"] = TRUE;
    $response["code"] = "-214";
    $response["msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>