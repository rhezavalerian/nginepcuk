<?php
 
require_once 'include/functions_logreg.php';
$db = new logreg();
$response = array("error" => FALSE);
 
if (isset($_POST['username']) && isset($_POST['npass']) && isset($_POST['cpass']) && isset($_POST['passlama'])) {
 
    $username = $_POST['username'];
    $npass = $_POST['npass'];
    $cpass = $_POST['cpass'];
    $passlama = $_POST['passlama'];

    if ($username == "" || $npass == "" || $cpass == "" ) {
        $response["error"] = TRUE;
        $response["code"] = "-600";
        $response["msg"] = "Slow down cowboy! .Pastikan anda telah mengisi semua form diatas! ";
        echo json_encode($response);
    }else{
            if($db->cekUserPassword($username,$passlama)) {
                $user = $db->gantiPassword($username, $npass);
                if ($user) {
                    $response["error"] = FALSE;
                    $response["code"] = "0";
                    $response["msg"] = "Perbuahan Password Berhasil!";
                    echo json_encode($response);
                } else {
                    $response["error"] = TRUE;
                    $response["code"] = "-603";
                    $response["msg"] = "Terjadi kesalahan saat melakukan perubahan password. Cek jaringan internet anda!";
                    echo json_encode($response);
                }
            } else{
                $response["error"] = TRUE;
                $response["code"] = "-602";
                $response["msg"] = "Ups sepertinya password lama anda tidak sesuai! ";
                echo json_encode($response);
            }
    }
} else {
    $response["error"] = TRUE;
    $response["code"] = "-605";
    $response["msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>