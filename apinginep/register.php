<?php
 
require_once 'include/functions_logreg.php';
$db = new logreg();
$response = array("error" => FALSE);
 
if (isset($_POST['nama']) && isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password'])) {
 
    $nama = $_POST['nama'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    if ($nama == "" || $username == "" || $email == "" || $password == "" ) {
        $response["error"] = TRUE;
        $response["code"] = "-200";
        $response["msg"] = "Slow down cowboy! .Pastikan anda telah mengisi semua form diatas! ";
        echo json_encode($response);
    }else{
        if ($db->cekEmail($email)) {
            $response["error"] = TRUE;
            $response["code"] = "-201";
            $response["msg"] = "Ups sepertinya Email " . $email . " telah digunakan oleh akun lain! ";
            echo json_encode($response);
        } else {
            if($db->cekUser($username)) {
                $response["error"] = TRUE;
                $response["code"] = "-202";
                $response["msg"] = "Ups sepertinya Username " . $username . " telah digunakan oleh akun lain! ";
                echo json_encode($response);
            } else{
            $user = $db->simpanUser($username, $nama, $email, $password);
            if ($user) {
                $response["error"] = FALSE;
                $response["code"] = "0";
                $response["msg"] = "Registrasi Berhasil!";
                $response["user"]["nama"] = $user["nama"];
                $response["user"]["username"] = $user["username"];
                $response["user"]["email"] = $user["email"];
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["code"] = "-203";
                $response["msg"] = "Terjadi kesalahan saat melakukan registrasi. Cek jaringan internet anda!";
                echo json_encode($response);
            }
            }
        }
    }
} else {
    $response["error"] = TRUE;
    $response["code"] = "-205";
    $response["msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>