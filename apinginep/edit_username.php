<?php
 
require_once 'include/functions_logreg.php';
$db = new logreg();
$response = array("error" => FALSE);
 
if (isset($_POST['usernamelama']) && isset($_POST['usernamebaru'])) {
 
    $usernamelama = $_POST['usernamelama'];
    $usernamebaru = $_POST['usernamebaru'];

    if ($usernamelama == "" || $usernamebaru == "") {
        $response["error"] = TRUE;
        $response["code"] = "-207";
        $response["msg"] = "Slow down cowboy! .Pastikan anda telah mengisi semua form diatas! ";
        echo json_encode($response);
    }else{
            if($db->cekUser($usernamebaru)) {
                $response["error"] = TRUE;
                $response["code"] = "-208";
                $response["msg"] = "Ups sepertinya Username " . $usernamebaru . " telah digunakan oleh akun lain! ";
                echo json_encode($response);
            } else{
            $user = $db->editUsername($usernamebaru, $usernamelama);
            if ($user) {
                $response["error"] = FALSE;
                $response["code"] = "0";
                $response["msg"] = "Username Berhasil Diubah!";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["code"] = "-209";
                $response["msg"] = "Terjadi kesalahan saat melakukan registrasi. Cek jaringan internet anda!";
                echo json_encode($response);
            }
            }
    }
} else {
    $response["error"] = TRUE;
    $response["code"] = "-206";
    $response["msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>