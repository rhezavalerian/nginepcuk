<?php
 
require_once 'include/functions_logreg.php';
$db = new logreg();
$response = array("error" => FALSE);
 
if (isset($_POST['emailbaru']) && isset($_POST['emaillama'])) {
 
    $usernamelama = $_POST['emaillama'];
    $usernamebaru = $_POST['emailbaru'];

    if ($usernamelama == "" || $usernamebaru == "") {
        $response["error"] = TRUE;
        $response["code"] = "-211";
        $response["msg"] = "Slow down cowboy! .Pastikan anda telah mengisi semua form diatas! ";
        echo json_encode($response);
    }else{
            if($db->cekEmail($usernamebaru)) {
                $response["error"] = TRUE;
                $response["code"] = "-212";
                $response["msg"] = "Ups sepertinya Email " . $usernamebaru . " telah digunakan oleh akun lain! ";
                echo json_encode($response);
            } else{
            $user = $db->editEmail($usernamebaru, $usernamelama);
            if ($user) {
                $response["error"] = FALSE;
                $response["code"] = "0";
                $response["msg"] = "Email Berhasil Diubah!";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["code"] = "-213";
                $response["msg"] = "Terjadi kesalahan saat melakukan registrasi. Cek jaringan internet anda!";
                echo json_encode($response);
            }
            }
    }
} else {
    $response["error"] = TRUE;
    $response["code"] = "-210";
    $response["msg"] = "Uh Oh Api yang anda gunakan sudah tidak valid!";
    echo json_encode($response);
}
?>